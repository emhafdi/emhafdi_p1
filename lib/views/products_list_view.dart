import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:emhafdi_p1/models/product_model.dart';
import 'package:emhafdi_p1/views/Product_Details.dart';




class SecondPage extends StatelessWidget {
  SecondPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text("Productos"),
        backgroundColor: Colors.purple,
      ),
      body: Center(
        child: FutureBuilder(
          future: Future.delayed(Duration(seconds: 3), () {
            return DefaultAssetBundle.of(context)
                .loadString('assets/img/products.json');
          }),
          builder: (BuildContext context, AsyncSnapshot<String?> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return CircularProgressIndicator();
            } else if (snapshot.hasError) {
              return Text('Error al cargar el archivo: ${snapshot.error}');
            } else if (snapshot.data == null) {
              return Text('el archivo esta vacio');
            } else {
              // Parsea la cadena JSON y crea una lista de productos
              List<Product> productos = _parseProducts(snapshot.data!);

              // Muestra la lista de productos
              return ListView.separated(
                padding: const EdgeInsets.all(5.0),
                itemBuilder: (context, index) =>
                    _buildProductListItem(context, productos[index]),
                separatorBuilder: (context, index) => const Divider(
                  thickness: 1,
                ),
                itemCount: productos.length,
              );
            }
          },
        ),
      ),
    );
  }

  Widget _buildProductListItem(BuildContext context, Product product) {
    return ListTile(
      leading: Hero(
        tag: '${product.name}',
        child: Image.asset(
          product.image,
          width: 50,
          height: 50,
        ),
      ),
      title: Text(product.name),
      subtitle: Text('\$${product.price.toStringAsFixed(2)}'),
      trailing: const Icon(Icons.chevron_right),
      onTap: () => _goToProductDetails(context, product),
    );
  }

  void _goToProductDetails(BuildContext context, Product product) {
    var route = MaterialPageRoute(
      builder: (context) => ProductDetails(product: product, Products:product,),
    );
    Navigator.of(context).push(route);
  }

 
  List<Product> _parseProducts(String jsonData) {
    List<dynamic> jsonList = json.decode(jsonData)['products'];
    List<Product> productos = jsonList.map((json) => Product.fromJson(json)).toList();
    return productos;
  }
}