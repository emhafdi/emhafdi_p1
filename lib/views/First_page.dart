import 'package:flutter/material.dart';
import 'package:emhafdi_p1/views/products_list_view.dart';



class FirstPage extends StatelessWidget {
  const FirstPage({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login',
      home: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                Spacer(
                  flex: 2,
                ),
                Container(
                  height: 150,
                  color: Colors.transparent,
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Login",
                    style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Spacer(
                  flex: 20,
                ),
                Container(
                  width: 40,
                  height: 150,
                  decoration: BoxDecoration(
                      color: Colors.purple,
                      shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.4),
                          spreadRadius: 5,
                          blurRadius: 15,
                          offset: Offset(0, 3),
                        ),
                      ]),
                ),
                Spacer(
                  flex: 2,
                ),
                Container(
                  width: 80,
                  height: 150,
                  decoration: BoxDecoration(
                      color: Colors.purple,
                      shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.4),
                          spreadRadius: 5,
                          blurRadius: 15,
                          offset: Offset(0, 3),
                        ),
                      ]),
                ),
                Container(
                  width: 20,
                  height: 150,
                  decoration: BoxDecoration(
                      color: Colors.purple,
                      shape: BoxShape.circle,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.4),
                          spreadRadius: 5,
                          blurRadius: 10,
                          offset: Offset(0, 3),
                        ),
                      ]),
                ),
                Spacer(
                  flex: 1,
                ),
              ]),
              Spacer(
                flex: 3,
              ),
              Container(
                width: 300,
                height: 200,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        spreadRadius: 10,
                        blurRadius: 9,
                        offset: Offset(0, 3),
                      ),
                    ]),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 250,
                      height: 60,
                      color: Colors.transparent,
                      child: TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                            prefixIcon: Icon(
                              Icons.person,
                              color: Colors.purple,
                            ),
                            labelText: 'Username',
                            labelStyle: TextStyle(fontWeight: FontWeight.bold)),
                      ),
                    ),
                    Container(
                      width: 250,
                      height: 60,
                      color: Colors.transparent,
                      child: TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                            prefixIcon: Icon(
                              Icons.vpn_key,
                              color: Colors.purple,
                            ),
                            labelText: 'Password',
                            labelStyle: TextStyle(fontWeight: FontWeight.bold)),
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(
                flex: 6,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () => _goToSecondPage(context),
                    child: Container(
                      width: 175,
                      height: 45,
                      decoration: BoxDecoration(
                          color: Colors.purple,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.horizontal(
                              left: Radius.circular(45)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.3),
                              spreadRadius: 5,
                              blurRadius: 15,
                              offset: Offset(0, 3),
                            ),
                          ]),
                      alignment: Alignment.center,
                      child: Text(
                        "LOGIN",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 22,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Spacer(
                flex: 6,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

void _goToSecondPage(BuildContext context) {
  var route = MaterialPageRoute(
    builder: (context) =>  SecondPage(),
  );
  Navigator.of(context).push(route);
}

