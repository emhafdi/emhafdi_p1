import 'package:emhafdi_p1/models/product_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:emhafdi_p1/models/product_model.dart';

class ProductDetails extends StatelessWidget {
  final Product product;

  ProductDetails({required this.product, required Product Products});

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Colors.white70,
        appBar: AppBar(
          title: const Text("Detalles del producto"),
          backgroundColor: Colors.purple,
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Spacer(
              flex: 1,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Hero(
                  tag: '${product.name}',
                  child: Container(
                    height: 350,
                    width: 325,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(image: AssetImage(product.image)),
                    ),
                  ),
                ),
              ],
            ),
            Spacer(
              flex: 1,
            ),
            Container(
              height: 350,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25),
                    topRight: Radius.circular(25)),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      product.name,
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                     "€ " + product.price.toString() ,
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      product.description,
                      style: TextStyle(
                          fontSize: 20, fontWeight: FontWeight.normal),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
}
